---
title: 7 - Stockage
separator: <!--h-->
verticalSeparator: \n---\n
highlightTheme: monokai
css: 'makina-theme/makina-formation.css' # Here
revealOptions:
  transition: 'slide'
  slideNumber: true
  fragmentInURL: true
---

<!-- .slide: class="title logo" -->

# Formation FullStack N7

## 7 - Stockage

**Mathieu DARTIGUES**

Développeur FullStack @MakinaCorpus

---

# Objectifs Stockage

* connaître les différents moteurs de stockage de données navigateur
* savoir utiliser `localStorage` et `sessionStorage`

<!--h-->

# Le stockage des données dans le navigateur

[Documentation MDN](https://developer.mozilla.org/en-US/docs/Web/API/Storage)

* [localStorage](https://developer.mozilla.org/en-US/docs/Web/API/Window/localStorage)
* [sessionStorage](https://developer.mozilla.org/en-US/docs/Web/API/Window/sessionStorage)
* [FileAPI non standard](https://developer.mozilla.org/en-US/docs/Web/API/File_and_Directory_Entries_API)
* [IndexedDB](https://developer.mozilla.org/en-US/docs/Web/API/IndexedDB_API)

---

**localStorage et sessionStorage**

* partagent la même API
* stockent de la donnée "simple" (pas d'objet, que des chaînes)
* sessionStorage n'est pas persisté après la fermeture du navigateur,
* localStorage est persisté
* quatre méthodes
  * setItem : définit une variable
  * getItem : lit une variable
  * removeItem : supprime une variable
  * clear : supprime toutes les variables

```js
localStorage.setItem('myCat', 'Tom');
const cat = localStorage.getItem('myCat');
localStorage.removeItem('myCat');
localStorage.clear();
```

---

<!-- .slide: class="alternate" -->

# Étape 11

**Consigne**

* persister les scores dans le `localStorage`
  * définir une clé unique par picross (`title`)
  * persister un tableau de `{ time, date }` où `time` est le temps réalisé, et `date` la date du jour
* afficher les temps du picross en dessous de "Victoire"

**Prérequis**

* modules JS (facultatif)
* localStorage
* manipulation du DOM

---
<!-- .slide: class="alternate" -->

# Bonus pour les motivés

* persister le score du joueur ssi il est un highscore
* demander le pseudo du joueur
* garder que les 5 meilleurs scores
* permettre au joueur de marquer les cellules comme "obligatoirement vide"
* styler votre application avec une librairie CSS utilitaire comme Tailwind CSS
* faire que ça s'affiche bien en RWD, et que ce soit jouable... (mais on est plus sur de la PWA là)
* compétition entre 2 utilisateurs en temps réel pour donner la victoire au plus rapide... en VueJS ?

<!--h-->

* [Revenir au sommaire](/slides/README.md#/2)
