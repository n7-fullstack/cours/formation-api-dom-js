---
title: N7 - Cours API DOM / JS
separator: <!--h-->
verticalSeparator: \n---\n
highlightTheme: monokai
css: 'makina-theme/makina-formation.css' # Here
revealOptions:
  transition: 'slide'
  slideNumber: true
  fragmentInURL: true
---

<!-- .slide: class="title logo" -->

# Formation FullStack N7

## Cours API DOM / JS

**Mathieu DARTIGUES**

Développeur FullStack @MakinaCorpus

---

# Présentations

---

# Vous

---

# Moi

<!--h-->

# Ce que l'on va réaliser

Des... nonogrammes !

[Démo](http://n7-nonogramme.surge.sh/)

Avec un navigateur, un IDE et un peu de code.

---

# Comment on va faire ?

* API DOM
  * on va manipuler les éléments du DOM
* API JS
  * on va aller chercher de la donnée à l'extérieur
  * on va gérer les interactions de l'utilisateur
  * on va écrire des algorithmes utilisant une écriture "moderne" du JS

---

# Comment j'interviens ?

* **j'explique les théories / concepts à connaître / comprendre**
* **je peux m'arrêter à tout moment**
  * quand **vous** en avez besoin
  * quand **vous** ne comprenez pas
  * quand **vous** avez une question
  * => **vous m'arrêtez** et on prend le temps d'expliquer / répondre
* **je relis votre code**
  * vous devez le publier sur un repo gitlab public et me donner l'accès
  * vous publierez également votre tp sur surge.sh

---

# Comment vous intervenez ?

* **vous corrigez les TPs**
  * à tour de rôle, vous partagez votre écran
  * vous montrez ce que vous avez fait
  * et vous essayez de l'expliquer
  * si vous n'avez pas réussi, ou si vous avez des difficultés, on corrige en co-construction
  * si vous ne souhaitez pas participer à cette section, dites le moi

<!--h-->

<!-- .slide: class="programme" -->

# Programme DOM

Les titres sont cliquables !

* [1. API DOM](/slides/1_DOM.md)
  * requêter le DOM pour obtenir des noeuds
  * comprendre les propriétés essentielles d'un noeud
  * savoir changer certaines propriétés d'un noeud

---

<!-- .slide: class="programme" -->

# Programme JavaScript (1/2)

La [documentation MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript)
est parfaitement adaptée pour l'apprentissage de JavaScript.

Nous reprenons ici beaucoup de documentation issue du portail MDN.

* [2. Histoire du JavaScript](/slides/2_JS_histoire.md)
  * connaître un résumé de l'histoire du JavaScript
* [3. Variables, boucle et tableaux](/slides/3_JS_primitifs.md)
  * connaître les différentes déclarations de variables
  * connaître les principaux types JavaScript
  * savoir debugger son code
  * connaître les différentes boucles
  * connaître les méthodes de manipulation des tableaux

---

<!-- .slide: class="programme" -->

# Programme JavaScript (2/2)

* [4. Asynchrone et syntaxe moderne](/slides/4_JS_moderne_async.md)
  * connaître les templates strings
  * connaître les arrow functions
  * connaître les opérateurs de destructuration
  * comprendre l'asynchrone
  * savoir faire une requête `fetch` en utilisant `async` / `await`
* [5. Prototype et classes](/slides/5_JS_prototype_classes.md)
* [6. Modules](/slides/6_JS_modules.md)
* [7. Stockage](/slides/7_JS_stockage.md)

---

## Détail des objectifs JavaScript (1/2)

* Les templates
  * template string
* Rappels sur le DOM
* La déstructuration
  * spread operator
  * destructured
  * object, array
  * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment
* Les opérateurs
  * les références
  * clone deep vs shallow copy
  * égalité, == vs ===
  * concaténation de chaîne
  * [opérateurs d'égalités](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Comparison_Operators)
* Les fonctions
* L'objet this
  * bind, call
  * apply
  * ...
* Les arrow function
* Les objets / prototypes / classes / constructeur
* Les modules `export` / `import` `default` / nommé
* L'asynchrone
  * async / await / Promise / callback
* Typologie de programmation en JavaScript
* Le stockage des données dans le navigateur
* La gestion des exceptions avec `try` / `catch`

