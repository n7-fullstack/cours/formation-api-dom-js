---
title: 5 - Prototype et classes
separator: <!--h-->
verticalSeparator: \n---\n
highlightTheme: monokai
css: 'makina-theme/makina-formation.css' # Here
revealOptions:
  transition: 'slide'
  slideNumber: true
  fragmentInURL: true
---

<!-- .slide: class="title logo" -->

# Formation FullStack N7

## 5 - Prototype et classes

**Mathieu DARTIGUES**

Développeur FullStack @MakinaCorpus

---

# Objectifs Prototype et classes

* connaître et savoir utiliser les classes
* connaître et comprendre le paradigme prototype

<!--h-->

<!-- .slide: class="bg-lion" -->

# Les classes JavaScript

---

Déclaration d'une classe JavaScript

[Documentation MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes)

* déclaration d'une classe : **`class`**
* définition d'un getter avec `get`
* définition d'un setter avec `set`
* champ / méthode `static`
  * permet l'usage d'une méthode sans instancier la classe

```js
class Polygon {
  static displayName = "Polygon";
  constructor(...sides) {
    this._sides = sides;
  }
  // Getter
  get area() {
    return this.calcArea();
  }
  // Method
  calcArea() {
    throw 'need to be implemented'
  }
}
```

---

* extension d'une classe avec `extends`
  * permet de bénéficier de la définition de la classe `Polygon`
  * le `constructor` de l'ancêtre est utilisable via `super()`
  * et d'ajouter / surcharger de nouvelles / existantes méthodes

```js
class Rectangle extends Polygon {
  static displayName = "Rectangle";
  constructor(l, L) {
    super(l, L, l, L)
    this._l = l
    this._L = L
  }
  calcArea() {
    return this._l * this._L;
  }
}

class Square extends Rectangle {
  static displayName = "Square";
  constructor(sideLength) {
    super(sideLength, sideLength)
  }
}
```

---

<!-- .slide: class="alternate" -->

# Étape 9

Transformer notre code avec une logique de classe

Une première pour l'éditeur `PicrossEditor`
* qui peut créer et injecter une table dans un noeud identifié
* qui met à jour le DOM au fur et à mesure de la construction
* qui permet de récupérer le JSON du picross créé

Une autre pour le jeu `PicrossGame`
* qui va "hériter" de `PicrossEditor`
* qui peut charger un picross
* présenter des fonctions utilitaires pour générer le DOM
* indiquer si le jeu est fini, ...
* comparer un tableau en entrée avec le picross courant et dire si ok / nok

Une troisième pour le `Timer`

<!--h-->

# Le paradigme prototype

Il est très proche du principe de l'héritage en langage objet.

En JavaScript, et grâce à la [chaîne du prototype](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Inheritance_and_the_prototype_chain),
tous les objets ont une hiérarchie de dépendance de prototypes,
qui se "termine" avec `null`.

`null` n'a pas de prototype, et il termine donc la chaîne. 

Une `function` permet de créer de nouvelles instances
grâce au mot clé `new`.

Un peu de lecture : https://jstutorial.medium.com/a-guide-to-prototype-based-class-inheritance-in-javascript-84953db26df0


---

<!-- .slide: class="alternate" -->

# Étape 9-bis

Transformer notre code avec une logique de prototype

Un premier pour l'éditeur `PicrossEditor`
* qui peut créer et injecter une table dans un noeud identifié
* qui met à jour le DOM au fur et à mesure de la construction
* qui permet de récupérer le JSON du picross créé

L'autre pour le jeu `PicrossGame`
* qui va "hériter" de `PicrossEditor`
* qui peut charger un picross
* présenter des fonctions utilitaires pour générer le DOM
* indiquer si le jeu est fini, ...
* comparer un tableau en entrée avec le picross courant et dire si ok / nok

---

Bonus, les [Proxy]([création de proxies](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy))

<!--h-->

* [Revenir au sommaire](/slides/README.md#/2)
* [6. Les modules](/slides/6_JS_modules.md)
