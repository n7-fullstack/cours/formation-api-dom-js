---
title: 2 - Histoire du JS
separator: <!--h-->
verticalSeparator: \n---\n
highlightTheme: monokai
css: '../makina-theme/makina-formation.css' # Here
revealOptions:
  transition: 'slide'
  slideNumber: true
  fragmentInURL: true
---

<!-- .slide: class="title logo" -->

# Formation FullStack N7

## 2 - Histoire du JS

**Mathieu DARTIGUES**

Développeur FullStack @MakinaCorpus

---

# Objectifs Histoire du JS

* Connaître un résumé de l'histoire du JavaScript
* Quelles sont les différences entre NodeJS et JavaScript

<!--h-->

Les début du World Wide Web... et de JavaScript

* *1990* : Naissance du World Wide Web par Tim Berners-Lee au CERN
* *1993* : Naissance du premier navigateur web, [NCSA Mosaic](<https://en.wikipedia.org/wiki/Mosaic_(web_browser)>)
* *1994* : Naissance du navigateur Netscape, nom de code Mozilla...
* *1995* : Brendan Eich **crée le language JavaScript** pour Netscape
* *1995* : Naissance d'Internet Explorer par Microsoft
* *1996* : Internet Explorer développe **JScript**, qui ressemble beaucoup à **JavaScript**
* *1997* : Naissance du standard ECMA-262, définissant ECMAScript, JavaScript étant une des implémentations
* *1998* : Libération du code source de Netscape... et création de Google et son algorithme **PageRank**

---

/poll qui est né avant 1990 ?

---

Les années 2000

* *2000* : Première version de [XHR](https://en.wikipedia.org/wiki/XMLHttpRequest) dans Internet Explorer 5/6
* *2002* : [Mozilla 1.0 !](http://www.mozillazine.org/articles/article2278.html)
* *2003* : [création de la Mozilla Foundation](https://blog.mozilla.org/press/2003/07/mozilla-org-announces-launch-of-the-mozilla-foundation-to-lead-open-source-browser-efforts/)
* *2004* : Utilisation intensive du XHR/Ajax sur le client mail GMail
* *2004* : [Firefox 1.0](https://blog.mozilla.org/press/2004/11/mozilla-foundation-releases-the-highly-anticipated-mozilla-firefox-1-0-web-browser/)
* *2006* : Intégration du XHR dans le W3C
* *2007* : lancement de l'iPhone, premier du nom
* *2008* : Arrivée de Google Chrome, le 2 Septembre
* *2009* : Le **moteur V8** de Chrome est rendu open-source
* *2009* : naissance de [**NodeJS**](https://en.wikipedia.org/wiki/Node.js) par [Ryan Dahl](https://en.wikipedia.org/wiki/Ryan_Dahl)
* *2009* : naissance d'un des premiers "gros" frameworks JavaScript, [AngularJS](https://github.com/angular/angular.js)
* *2010* : création du registre **npm**

---

La décénie 2010 - 2020

* *2012* : naissance de [TypeScript](https://www.typescriptlang.org/)
* *2013* : début du projet Firefox OS
* *2013* : naissance de [ReactJS](https://reactjs.org/)
* *2014* : naissance de [VueJS](https://vuejs.org/)
* *2015* : apparition du terme **Progressive Web App** avec Google
* *2016* : sortie annuelle des versions d'ECMAScript par le [TC39](https://tc39.es/)
* *2016* : évolution d'AngularJS en [Angular](https://angular.io)
* *2018* : naissance de [**Deno**](https://deno.land/) par... Ryan Dahl !
* *2019* : version 3 de [Svelte](https://svelte.dev/)
* ...

---

# Différences JavaScript / NodeJS

---

* **JavaScript** est un **langage de programmation multi-paradigme** conforme à la spécification ECMAScript.

  * principalement utilisé côté **client** et interprété par un navigateur
  * peut aussi être utilisé côté **serveur** (via l'environnement NodeJS)
  * il respecte les paradigmes objet, fonctionnel, impératif, et événementiel

---

* **NodeJS** représente :
  * un **environnement d’exécution** (runtime)
  * **un ensemble d’API** JavaScript
  * une **machine virtuelle** (VM) JavaScript performante
    (parseur, interpréteur et compilateur)
    pouvant accéder à des ressources système telles que des fichiers (filesystem)
    ou des connexions réseau (sockets).
  * **Node.js** est la propriété de l'entreprise **Joyent**,
    mais sa gouvernance est gérée par **la Node.js Foundation**.

---

# Évolution de la specification ECMAScript

---

* JavaScript est une **implémentation du standard ECMAScript**
* ECMAScript est un ensemble de normes concernant les langages de programmation de type script
 (JavaScript, ActionScript, JScript,...), standardisées dans le cadre de la specification **ECMA-262**
* La version supportée par la plupart des navigateurs aujourd'hui est ECMAScript 5 (ES5)
* Le support pour la version 6 (ES2015) est très correcte chez les navigateurs "evergreen" (moderne, sauf IE 11)
* La version **ESnext**, étant la version de développement est [partiellement supportée](http://kangax.github.io/compat-table/esnext/) (même avec Babel)

---

![](./assets/carbon.png)

---

* Aujourd'hui, on peut donc écrire une application pour le web :
  * soit en **ES5**
  * soit en **ES6 (et/ou ES7) + Babel**
  * soit en **ESnext**, sans Babel en faisant attention aux navigateurs cibles
---

![](./assets/ecmascript.svg)

<!--h-->

* [Revenir au sommaire](/slides/README.md#/2)
* [3. Types primitifs](/slides/3_JS_primitifs.md)
