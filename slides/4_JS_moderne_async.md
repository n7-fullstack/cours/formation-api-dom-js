---
title: 4 - Modernité et asynchrone
separator: <!--h-->
verticalSeparator: \n---\n
highlightTheme: monokai
css: '../makina-theme/makina-formation.css' # Here
revealOptions:
  transition: 'slide'
  slideNumber: true
  fragmentInURL: true
---

<!-- .slide: class="title logo" -->

# Formation FullStack N7

## 4 - Modernité et asynchrone

**Mathieu DARTIGUES**

Développeur FullStack @MakinaCorpus

---

# Objectifs Modernité et asynchrone

* connaître les templates strings
* connaître les arrow functions
* connaître les opérateurs de destructuration
* comprendre l'asynchrone
* savoir faire une requête `fetch` en utilisant `async` / `await`

<!--h-->

# Les templates strings

[Documentation MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals)

En ES5

```js
var maChaine = 'Toto';
var maChaineComplete = 
    'Je m\'appelle ' + maChaine +
    ' et je préfère l\'école buissonière';
```

En ES6

```js
let maChaine = 'Toto';
let maChaineComplete = `Je m'appelle ${maChaine} 
                        et je préfère l'école buissonière`;
```

<!--h-->

# Les arrow function

[Documentation MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions)

En ES5

```js
var odds = evens.map(function (v) {
    return v + 1;
});
```

En ES6

```js
const odds = evens.map(v => v + 1);
```

---

Comment on en est arrivé là ?

```js
// Traditional Function
function (a){
  return a + 100;
}

// Arrow Function Break Down

// 1. Remove the word "function" and place arrow between the argument and opening body bracket
(a) => {
  return a + 100;
}

// 2. Remove the body brackets and word "return" -- the return is implied.
(a) => a + 100;

// 3. Remove the argument parentheses
a => a + 100;
```

---

# this

[Documentation MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/this)

`this` n'est pas toujours l'instance de l'objet sur lequel nous utilisons des méthodes.

Il correspond plutôt au contexte d'appel d'une fonction.

Dans notre cas, vu que nous sommes dans un navigateur,
`this` pourrait être l'objet [`Window`](https://developer.mozilla.org/en-US/docs/Web/API/Window).

Nous pouvons le modifier en utilisant
[`.bind()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/bind)
sur la fonction que nous appelons,
ce qui nous permettra de connaître à l'avance
ce que sera `this`.

L'arrow function nous permet de mieux maîtriser ce `this`.

---

En ES5

```js
var bob = {
    _name: "Bob",
    _friends: ["Jeanne", "Rachid", "Myriam"],
    printFriends: function printFriends() {
      
        var _this = this;

        this._friends.forEach(function (f) {
            return console.log(_this._name + " knows " + f);
        });
    }
};
```

---

En ES6

```js
const bob = {
    _name: "Bob",
    _friends: ["Jeanne", "Rachid", "Myriam"],
    printFriends() {
        this._friends.forEach(f => {
            console.log(this._name + " knows " + f));
        }
    }
};
```

Un article pour mieux comprendre https://yehudakatz.com/2011/08/11/understanding-javascript-function-invocation-and-this/

---

<!-- .slide: class="alternate" -->

# Étape 4 : construire un objet contenant les instructions d'un nonogramme

Rappeler définition d'un nonogramme

**Consignes**

* construire un objet JavaScript qui contient
  * pour chaque ligne
  * et pour chaque colonne
  * les séquences de chiffres permettant d'arriver au dessin en sortie
  * voir propriétés indiquées page suivante
* afficher cet objet dans un élément `pre`
* enregistrer ce fichier sur son poste en tant que 'picross1.json'

**Prérequis**

* fonction en notation raccourcie, arrow function (facultatif)
* manipulation d'un tableau JavaScript `Array`
* template string (facultatif)
* manipulation du DOM
* algorithmique !
<!-- utiliser `innerHTML`, propriété d'un `Node` DOM -->

<!--
Pièges à éviter

* renseigner la propriété `size` avec les nombres de lignes / colonnes 'corrects', càd issues de l'élément `table` et de ses `tr` / `td` au lieu de se baser sur les champs de saisies qui peuvent changer dans le temps

-->

---

<!-- .slide: class="alternate" -->


```
  {
    "title": "Le titre du nonogramme",
    "data": {
      "rows": [
        [ 1, 2, 3 ], // la première ligne contient 1 puis 2 puis 3 cases "allumées"
        [ 1 ], // la deuxième ligne uniquement une case
        ...
      ],
      "columns": [
        [ 1, 2, 3 ], // la première colonne contient 1 puis 2 puis 3 cases "allumées"
        [ 1 ], // la deuxième colonne uniquement une case
        ...
      ]
    },
    "size": {
      "rows": 10,
      "columns": 10
    }
  }
```

---

<!-- .slide: class="alternate" -->

Détails de l'algorithme (1/2)

**Première étape, recensement de l'état des cellules par ligne et colonnes**
* créer un `Array` pour les `cellsStateByRow`
* créer un `Array` pour les `cellsStateByColumn`
* pour chaque ligne du tableau DOM
  * créer une nouvelle `currentCellsStateRow`
  * pour chaque (`cell`, `cellIndex`) de la ligne courante
    * créer une nouvelle entrée dans `cellsStateByColumn` s'il n'y a pas de colonne correspondante à l'index `cellIndex` de la `cell`
    * ajouter l'état de la `cell` dans la `cellsStateByColumn[cellIndex]`
    * ajouter l'état de la `cell` dans la `currentCellsStateRow`
  * ajouter la `currentCellsStateRow` à `cellsStateByRow`

---

<!-- .slide: class="alternate" -->

Détails de l'algorithme (2/2)

**Deuxième étape, on calcule les enchaînements de cellules activées**
* création d'un tableau pour les `rowsIndications`
* pour chaque `currentCellsStateRow` de `cellsStateByRow`
  * créer une nouvelle `rowIndication`
  * initialisation d'une variable `cellsEnabledNumber` à 0
  * pour chaque valeur de `currentCellsStateRow` (qui correspond à une cellule)
    * si la valeur est "active", on ajoute 1 à `cellsEnabledNumber`
    * sinon
      * si `cellsEnabledNumber` > 0 on l'ajoute à `rowIndication`
      * on remet à 0 `cellsEnabledNumber`
  * si `cellsEnabledNumber` > 0 on l'ajoute à `rowIndication`
  * on ajoute `rowIndication` à `rowsIndications`

Idem pour les colonnes

<!--h-->

# Les opérateurs de destructuration

[Documentation](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment)

**Opérateur Spread**

En ES5

```js
var smallList = [ 'a', 'b', 'c' ];
var bigList = [].concat(smallList, [ 'd', 'e' ]);
```

En ES6

```js
const smallList = ['a', 'b', 'c'];
const bigList = [...smallList, 'd', 'e'];
```

---

**Opérateur Rest**

```js
let a, b, rest;
[a, b] = [10, 20];
console.log(a); // 10
console.log(b); // 20

[a, b, ...rest] = [10, 20, 30, 40, 50];
console.log(a); // 10
console.log(b); // 20
console.log(rest); // [30, 40, 50]

({ a, b } = { a: 10, b: 20 });
console.log(a); // 10
console.log(b); // 20


// Stage 4(finished) proposal
({a, b, ...rest} = {a: 10, b: 20, c: 30, d: 40});
console.log(a); // 10
console.log(b); // 20
console.log(rest); // {c: 30, d: 40}
```

---

<!-- .slide: class="alternate" -->

# Étape 4.1 : ajouter de la destructuration

**Consignes**

* utiliser l'opérateur spread pour déstructurer le résultat de la fonction `computeDataTable`
* créer un fichier JSON `picross.json` (ou autre) qui recensera au moins un nonogramme de votre production (vous pouvez vous inspirer d'images sur https://duckduckgo.com/?q=picross&ia=images&iax=images)

<!--h-->

# L'asynchrone

JavaScript est un langage qui peut être asynchrone.

Asynchrone est l'inverse de synchrone.

Des instructions synchrones s'exécutent les unes à la suite des autres.

La deuxième attend que la première est terminée avant de commencer.

Des instructions asynchrones ne s'attendent pas. Elles s'exécutent en parallèle.

Ce qui peut devenir trompeur, car à la lecture,
nous pourrions penser que deux lignes qui se suivent s'exécuteraient en séquence.

---

Quelques méthodes asynchrones :

* [`XmlHTTPRequest`](https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest/Using_XMLHttpRequest)
* [`setTimeout`](https://developer.mozilla.org/en-US/docs/Web/API/WindowOrWorkerGlobalScope/setTimeout) (et son pendant `clearTimeout`)
* [`setInterval`](https://developer.mozilla.org/en-US/docs/Web/API/WindowOrWorkerGlobalScope/setInterval) (et son pendant `clearInterval`)
* [`fetch`](https://developer.mozilla.org/en-US/docs/Web/API/WindowOrWorkerGlobalScope/fetch)


---

Exemples de code avec un `setTimeout`

```js
setTimeout(function maisTOu() {
  console.log('Je suis là !!!')
}, 1000)
console.log('Mais t pas là ? t où ?')
```

---

La gestion de cet asynchronisme s'est effectuée initialement
avec des `callbacks`, fonction de rappel en français.

Ça a généré, parfois, du code spaghetti appelé
["callback hell"](http://callbackhell.com/).

```js
fs.readdir(source, function (err, files) {
  if (err) {
    console.log('Error finding files: ' + err)
  } else {
    files.forEach(function (filename, fileIndex) {
      console.log(filename)
      gm(source + filename).size(function (err, values) {
        if (err) {
          console.log('Error identifying file size: ' + err)
        } else {
          console.log(filename + ' : ' + values)
          aspect = (values.width / values.height)
          widths.forEach(function (width, widthIndex) {
            height = Math.round(width / aspect)
            console.log('resizing ' + filename + 'to ' + height + 'x' + height)
            this.resize(width, height).write(dest + 'w' + width + '_' + filename, function(err) {
              if (err) console.log('Error writing file: ' + err)
            })
          }.bind(this))
        }
      })
    })
  }
})
```

---

Au fur et à mesure des usages, 
ECMAScript a évolué et proposé d'autres manières de gérer
les retours de méthodes asynchrones.

[**Promesses**](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise)
  * `Promise`
  * `Promise.all()`
  * `Promise.resolve()`
  * `Promise.reject()`
  * `Promise.prototype.then()`
  * `Promise.prototype.catch()`
  * `Promise.prototype.finally()`

[**`async`**](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/async_function)/[**`await`**](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/await)

---

<!-- .slide: class="alternate" -->

Exemple avec `Promise.then`

```js
function resolveAfter2Seconds() {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve('allô ?');
    }, 2000);
  });
}

function asyncCall() {
  console.log('ça sonne...');
  return resolveAfter2Seconds()
    .then(message => {
      console.log(message)
    })
}

asyncCall().then(() => {
  console.log('ça fait au moins 2 secondes que tu aurais dû répondre !!')
})
```

---

<!-- .slide: class="alternate" -->

Exemple avec `async / await`

```js
function resolveAfter2Seconds() {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve('allô ?');
    }, 2000);
  });
}

async function asyncCall() {
  console.log('ça sonne...');
  const result = await resolveAfter2Seconds();
  console.log(result);
}

await asyncCall();
console.log('ça fait au moins 2 secondes que tu aurais dû répondre !!')
```

---

Usage de fetch avec Promise et async/await

```js
// Promise
fetch(myRequest)
  .then(response => response.json())
  .then(data => {
    /* process your data further */
  })
  .catch(error => console.error(error));

// in an async function
// async / await
try {
  const response = await fetch(myRequest)
  const data = await response.json();
  /* process your data further */
} catch (error) {
  console.error(error)
}

```

---

<!-- .slide: class="alternate" -->

# Étape 5

**Consignes**

* présenter au joueur l'ensemble des nonogrammes à sa disposition
  * charger le fichier `picross.json` précédemment créé
  * injecter dans le DOM une liste d'élément qui présente les nonogrammes

**Prérequis**

* connaître `fetch`
* connaître `async` / `await` et/ou `Promise.then` / `Promise.catch`
* disposer d'un serveur web qui permet d'exposer le fichier JSON construit
  * [http-server](https://www.npmjs.com/package/http-server)
  * [Live Server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer)

---

<!-- .slide: class="alternate" -->

# Étape 6

**Consignes**

* permettre au joueur de cliquer sur un des picross,
* et initialiser le tableau avec la grille **et les indices de remplissage**
* corriger la fonction `computeDataTable`

**Prérequis**

* utiliser la destructuration de tableaux

---

<!-- .slide: class="alternate" -->

# Étape 7

**Consignes**

* détecter quand le joueur a gagné
* utilisation d'une comparaison "totale" (comparaison des matrices des tables)
* informer le joueur qu'il a gagné

**Prérequis**

* utiliser la destructuration
* comparaison de valeurs de tableau
* manipulation des tableaux avec `join`

---
# Étape 8

**Consignes**

* ajouter un chronomètre et le déclencher au démarrage du choix d'un picross
* afficher l'évolution du chronomètre
* arrêter le chronomètre lorsque le joueur a gagné

**Prérequis**

* utiliser l'asynchrone avec un `setInterval` et `clearInterval`


<!--h-->

* [Revenir au sommaire](/slides/README.md#/2)
* [5. Prototype et classes](/slides/5_JS_prototype_classes.md)
