---
title: 1 - API DOM
separator: <!--h-->
verticalSeparator: \n---\n
highlightTheme: monokai
css: '../makina-theme/makina-formation.css' # Here
revealOptions:
  transition: 'slide'
  slideNumber: true
  fragmentInURL: true
---

<!-- .slide: class="title logo" -->

# Formation FullStack N7

## 1 - API DOM

**Mathieu DARTIGUES**

Développeur FullStack @MakinaCorpus


---

# Objectifs DOM

* Requêter le DOM pour obtenir des noeuds
* Comprendre les éléments essentiels d'un noeud
* Savoir changer certaines propriétés d'un noeud

<!--h-->

# Qu'est ce que le DOM ?

[définition MDN](https://developer.mozilla.org/fr/docs/Web/API/Document_Object_Model)

Le **Document Object Model** ou DOM (pour modèle objet de document)
est une **interface de programmation** pour les documents **HTML, XML et SVG**.
Il fournit une représentation structurée du document sous forme d'un **arbre**
et définit la façon dont la structure peut être manipulée par les programmes,
en termes de **style** et de **contenu**.

Le DOM représente le document comme un **ensemble de nœuds et d'objets**
possédant des **propriétés** et des **méthodes**.
Les nœuds peuvent également avoir des **gestionnaires d'événements**
qui se déclenchent lorsqu'un événement se produit.
Cela permet de manipuler des pages web grâce à des scripts
et/ou des langages de programmation.

Les nœuds peuvent être associés à des gestionnaires d'événements.
Une fois qu'un événement est déclenché, les gestionnaires d'événements sont exécutés.

<!--h-->

<!-- .slide: class="alternate" -->

# Étape 1.1 : Créer une `Table`


**Consignes**

* afficher un tableau de 6 x 6 avec uniquement du HTML, pas de JavaScript

**Questions**

* quels sont les éléments HTML que nous devons utiliser ?
* construire le tableau de 6 x 6 avec les éléments de la question précédente

---

## Comment "voir" le DOM ?

* utilisation du DevTools (Firefox / Chromium-based)
* manipulation dans la console
  * querySelector / querySelectorAll

---

<!-- .slide: class="alternate" -->

# Étape 1.2 : Requêter les `Element` de notre page

**Consignes**

* afficher dans la console du navigateur l'élément tableau
* afficher l'ensemble des `tr` de notre tableau


---

## Comment manipuler le DOM ?

* modification des styles d'un noeud du DOM
* modification des classes d'un noeud du DOM
* sibling, className, NodeList, Element, nodeName
* EventListener, [addEventListener](https://developer.mozilla.org/fr/docs/Web/API/EventTarget/addEventListener)
* injection d'éléments DOM
  * document.createElement
  * document.getElementById
  * Element.appendChild

---

<!-- .slide: class="alternate" -->

# Étape 1.3 : Modifier l'aspect d'une cellule

**Consignes**

* changer l'apparence d'une cellule au survol en lui changeant son fond en rouge
* activer une cellule en persistant un fond bleu (ou autre couleur de votre choix) lorsque le joueur clique dessus
* si le joueur reclique sur une cellule déjà "activée", enlever sa couleur de fond "activée"

**Questions**

* sur quel / quels éléments devons nous "attacher" l'événement pour changer la couleur du fond
* comment changer "facilement" l'apparence d'une cellule ?

<!--h-->

* [Revenir au sommaire](/slides/README.md#/2)
* [2 Histoire du JavaScript](/slides/2_JS_histoire.md)