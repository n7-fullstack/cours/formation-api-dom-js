---
title: 3 - Variables, boucle et tableaux
separator: <!--h-->
verticalSeparator: \n---\n
highlightTheme: monokai
css: '../makina-theme/makina-formation.css' # Here
revealOptions:
  transition: 'slide'
  slideNumber: true
  fragmentInURL: true
---

<!-- .slide: class="title logo" -->

# Formation FullStack N7

## 3 - Variables, boucle et tableaux

**Mathieu DARTIGUES**

Développeur FullStack @MakinaCorpus

---

# Objectifs Variables, boucle et tableaux

* connaître les différentes déclarations de variables
* connaître les principaux types JavaScript
* savoir debugger son code
* connaître les différentes boucles
* connaître les méthodes de manipulation des tableaux

<!--h-->

# La déclaration de variables

Plusieurs mots clés sont à notre disposition.

`let`, `var` et `const`.

Leurs différences se situent au niveau de la "portabilité" de la variable.

En ES5, pour déclarer une variable on utilisait `var`.
En ES6, il y a 2 types de variables :

* **var** : une variable qui est mutable et ["hoistée"](https://developer.mozilla.org/en-US/docs/Glossary/Hoisting) aux scopes supérieurs    
* **let** : une variable qui est mutable
* **const** : une variable immutable (attention aux objets cependant, il s'agit de la référence de la variable)

Les portées des variables `let` et `const` sont limitées
au scope dans lequel elles ont été déclarées ainsi qu'à tous les scopes enfants.

Alors que les variables `var` "remontent" les scopes.

---

<!-- .slide: class="alternate" -->

# Essais de déclarations de variables

```js
let maVariableLet = 'a';
const maVariableConstante = 'b';
{
    console.log(maVariableLet); // 'a'
    console.log(maVariableConstante); // 'b'
    console.log(maVariableVar); // undefined alors qu'elle n'a pas encore été déclarée, mais finalement sa déclaration est bien faite "avant l'heure"
    var maVariableVar = 'c';
    maVariableLet = 'ab';
    // maVariableConstante = 'bc'; // ! impossible !
    maVariableVar = 'cd';
    {
        // console.log(maVariableLet); // undefined OU impossible
        let maVariableLet = 'aLocale';
        console.log(maVariableLet); // 'aLocale'
    }
    console.log(maVariableLet); // 'ab'
    console.log(maVariableConstante); // 'b'
    console.log(maVariableVar); // 'cd'
}
console.log(maVariableLet); // 'ab'
console.log(maVariableConstante); // 'c'
console.log(maVariableVar); // 'cd', on est au scope au dessus
```

<!--h-->

# Les principaux types JavaScript

En JavaScript, tout est objet sauf les primitifs.

[Les primitifs](https://developer.mozilla.org/en-US/docs/Glossary/Primitive)
sont au nombre de 6 : `boolean`, `number`, `bigint`, `undefined`, `symbol` et `string`. Ils sont immutables. (ça n'empêche pas qu'on peut muter une variable qui aurait pour valeur un primitif)

Tous les autres objets "descendent", ou "héritent" de `null`
grâce à la chaîne des prototypes.

* Boolean
* BigInt
* Number
* String
* Symbol
* Object
* Array
* Map
* Set
* WeakMap
* WeakSet
* Function

---

<!-- .slide: class="alternate" -->

## Essais de variables Objet

**Consignes**

* dans la console du navigateur,
* déclarer un objet `monIdentité` qui a pour propriété `nom` et `prénom`
* chacune de ces propriétés a pour valeur votre nom et votre prénom
* afficher uniquement votre nom, puis uniquement votre prénom

**Questions**

* quel est l'opérateur qui nous permet d'accéder à une propriété d'un objet ?
* quel est la nature de l'objet que nous devons créer ? mutable ou immutable ?

<!--
* le point
* immutable, votre identité ne doit pas changer ?

valider le fait que, même immutable, les propriétés d'un objet restent mutables...
-->

---

Pour empêcher qu'un développeur ou que durant l'exécution,
une propriété d'un objet ne change, vous pouvez le [`freeze`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/freeze).

`Object.freeze(monIdentité)` devrait vous empêcher de modifier votre nom...

silencieusement, sauf si vous ajoutez un `use strict`.

<!--h-->

# Savoir Debugger son code

* `debugger`
* `console.log`
* `console.warn`
* `console.info`
* `console.error`
* `console.dir`
* `console.table`
* `console.group`

https://developer.mozilla.org/en-US/docs/Web/API/Console/


Apprendre à debugger, avec l'instruction `debugger`

<!--h-->

<!-- .slide: class="alternate" -->

# Connaître les différentes boucles

[do while](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/do...while)

```js
let result = '';
let i = 0;

do {
  i = i + 1;
  result = result + i;
} while (i < 5);

console.log(result);
// expected result: "12345"
```

[while](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/while)

```js
let n = 0;

while (n < 3) {
  n++;
}

console.log(n);
// expected output: 3
```

---

<!-- .slide: class="alternate" -->

[for](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for) pour les boucles "à l'ancienne"

```js
let str = '';

for (let i = 0; i < 9; i++) {
  str = str + i;
}

console.log(str);
// expected output: "012345678"
```

---

<!-- .slide: class="alternate" -->

[for in](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for...in) pour le parcours des propriétés d'un objet

```js
const object = { a: 1, b: 2, c: 3 };

for (const property in object) {
  console.log(`${property}: ${object[property]}`);
}

// expected output:
// "a: 1"
// "b: 2"
// "c: 3"
```

[for of](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for...of) et [for await of](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for-await...of) pour le parcours des éléments d'un tableau

```js
const array1 = ['a', 'b', 'c'];

for (const element of array1) {
  console.log(element);
}

// expected output: "a"
// expected output: "b"
// expected output: "c"
```

---

<!-- .slide: class="alternate" -->

# Étape 2 Injection du DOM avec du JS

**Consignes**

* ajouter un formulaire de saisie d'un nombre de colonne et de lignes
* ajouter un bouton de création de tableau
* à partir des données saisies par le joueur, et après clic sur le bouton, procéder à la création et affichage du tableau
* si le joueur change  les nombres et reclique sur le bouton, le tableau est remplacé par un nouveau
* remettre en place la gestion des cellules de l'étape 1

**Questions**

* quel(s) élément(s) DOM devons nous utiliser pour disposer d'un champ de saisie ?
* comment faire pour déclencher la création du tableau ?
* quelles méthodes de l'API DOM devons nous utiliser pour créer ce tableau

<!--
  * input type=text
  * avoir un événement onclick sur le bouton, ou addEventListener
  * document.createElement, Element.appendChild
-->

**Prérequis**

* déclaration de variable
* boucle avec `for` et `while`
* manipulation du DOM
* gestion des événements du DOM

<!--h-->

# Manipulation des tableaux

Les tableaux sont des `Array` en JavaScript.

Un tableau est une liste d'éléments, indexés à partir de `0`
(ce sera le premier élément du tableau).

```js
const monTableau = ['voiture', 'moto', 'vélo', 'trotinette'];
```

Pour accéder à un élément du tableau, la syntaxe `[index]`
permet d'obtenir l'élément en position n° `index.

```js
console.log(monTableau[2]); // affichera 'vélo'
```

Ils disposent d'un ensemble de méthodes de manipulation très large,
nous allons en voir quelques unes des plus utilisées.

N'hésitez pas à vous référer au [portail MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array)

---

<!-- .slide: class="alternate" -->

`Array.length` renvoie la taille du tableau

[`Array[index]`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/length)
permet d'accéder à l'élément situé à l'`index` du tableau

```js
let fruits = ['Apple', 'Banana']

console.log(fruits.length)
// 2

let first = fruits[0]
// Apple

let last = fruits[fruits.length - 1]
// Banana
```

---

<!-- .slide: class="alternate" -->

[`Array.forEach()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/forEach)
permet de parcourir l'ensemble des éléments d'un tableau


```js
const array1 = ['a', 'b', 'c'];

array1.forEach(element => console.log(element));

// expected output: "a"
// expected output: "b"
// expected output: "c"
``` 

[`Array.map()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map)
permet de parcourir l'ensemble des éléments d'un tableau, et de construire un nouveau tableau constitué d'élément transformé

```js
const array1 = [1, 4, 9, 16];

// pass a function to map
const map1 = array1.map(x => x * 2);

console.log(map1);
// expected output: Array [2, 8, 18, 32]
```

---

<!-- .slide: class="alternate" -->

[`Array.find()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/find)
et [`Array.findIndex()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/findIndex)
recherche des éléments dans le tableau, et renvoie soit l'élément, soit l'index de l'élément dans le tableau

```js
const array1 = [5, 12, 8, 130, 44];

const found = array1.find(element => element > 10);

console.log(found);
// expected output: 12

const isLargeNumber = (element) => element > 13;

console.log(array1.findIndex(isLargeNumber));
// expected output: 3

```

---

<!-- .slide: class="alternate" -->

[`Array.push()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/push) permet d'ajouter un élément dans un tableau "in place"

```js
const animals = ['pigs', 'goats', 'sheep'];

const count = animals.push('cows');
console.log(count);
// expected output: 4
console.log(animals);
// expected output: Array ["pigs", "goats", "sheep", "cows"]

animals.push('chickens', 'cats', 'dogs');
console.log(animals);
// expected output: Array ["pigs", "goats", "sheep", "cows", "chickens", "cats", "dogs"]
```

---

<!-- .slide: class="alternate" -->

[`Array.splice()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/splice) supprime / ajoute / met à jour un ou plusieurs éléments d'un tableau

```js
const months = ['Jan', 'March', 'April', 'June'];
months.splice(1, 0, 'Feb');
// inserts at index 1
console.log(months);
// expected output: Array ["Jan", "Feb", "March", "April", "June"]

months.splice(4, 1, 'May');
// replaces 1 element at index 4
console.log(months);
// expected output: Array ["Jan", "Feb", "March", "April", "May"]
```

---

<!-- .slide: class="alternate" -->

[`Array.filter()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter) permet de filtrer les éléments d'un tableau, renvoie un nouveau tableau des éléments filtrés sans altérer le tableau d'origine

```js
const words = ['spray', 'limit', 'elite', 'exuberant', 'destruction', 'present'];

const result = words.filter(word => word.length > 6);

console.log(result);
// expected output: Array ["exuberant", "destruction", "present"]
```

---

<!-- .slide: class="alternate" -->

[`Array.sort()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort) permet de trier les éléments d'un tableau **"in place"**, cette méthode **mute** le tableau sur lequel vous effectuez le tri... attention...

```js
const months = ['March', 'Jan', 'Feb', 'Dec'];
months.sort();
console.log(months);
// expected output: Array ["Dec", "Feb", "Jan", "March"]

const array1 = [1, 30, 4, 21, 100000];
array1.sort();
console.log(array1);
// expected output: Array [1, 100000, 21, 30, 4]
```

---

<!-- .slide: class="alternate" -->

[`Array.reduce()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/reduce) réduit un tableau à une seule valeur, calculée avec une fonction de réduction et un accumulateur

```js
const array1 = [1, 2, 3, 4];
const reducer = (accumulator, currentValue) => accumulator + currentValue;

// 1 + 2 + 3 + 4
console.log(array1.reduce(reducer));
// expected output: 10

// 5 + 1 + 2 + 3 + 4
console.log(array1.reduce(reducer, 5));
// expected output: 15
```

---

<!-- .slide: class="alternate" -->

# Étape 3 : Générer un nonogramme en JSON

**Consignes**

* ajouter un input permettant la saisie d'un "titre" du nonogramme
* construire un tableau mémoire (`Array`) à partir des cellules activées
  * le tableau contiendra autant d'élément que de lignes `tr` que la `table` du DOM en contient
  * chacun des éléments de ce tableau contiendra à son tour autant de valeurs booléen (`true` pour une cellule activée, `false` sinon) que de `td` dans la `tr` courante
* construire un objet JSON qui contiendra l'ensemble
* l'afficher dans la console

**Prérequis**

* manipulation d'un tableau JavaScript `Array`
* parcours des éléments DOM

**Résultat attendu**

```json
{
  "title": "mon nonogramme", 
  "data": [ 
    [ true, false ],
    [ false, false ]
  ]
}
```

<!--h-->

* [Revenir au sommaire](/slides/README.md#/2)
* [4. Asynchrone et modernité](/slides/4_JS_moderne_async.md)
