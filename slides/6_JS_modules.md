---
title: 6 - Les modules
separator: <!--h-->
verticalSeparator: \n---\n
highlightTheme: monokai
css: 'makina-theme/makina-formation.css' # Here
revealOptions:
  transition: 'slide'
  slideNumber: true
  fragmentInURL: true
---

<!-- .slide: class="title logo" -->

# Formation FullStack N7

## 6 - Les modules

**Mathieu DARTIGUES**

Développeur FullStack @MakinaCorpus

---

# Objectifs Modules

* connaître l'histoire des modules JavaScript
* connaître la syntaxe des modules et savoir l'utiliser

<!--h-->

# Un peu d'histoire

Au début, JavaScript était utilisé uniquement pour du script,
des petites tâches.

Petit à petit, son usage a grossit, grossit,
jusqu'à ce que nous, les développeurs, fabriquions des vrais logiciels avec,
ou de vrais programmes côté serveur avec NodeJS.

Le besoin en découpage s'est fait petit à petit ressentir,
et plusieurs initiatives ont existé : AMD, CommonJS, UMD...
Par exemple, pour respecter le premier des 5 principes de la théorie [**SOLID**](https://en.wikipedia.org/wiki/SOLID),
le [**Single Responsability Principle**](https://en.wikipedia.org/wiki/Single-responsibility_principle).

Aujourd'hui, le standard ECMAScript définit précisément la gestion des modules,
et autant les navigateurs (récents) que les interpréteurs côté serveur
respectent cette définition.

[Détail des anciennes syntaxes (AMD, CommonJS, UMD)](https://www.davidbcalhoun.com/2014/what-is-amd-commonjs-and-umd/)

[Documentation MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Modules)

---

# Syntaxe des modules

**export**

Si on fait un `export` sans spécifier de default :

```js
export monModule
```

Alors il faudra l'importer avec des { }

```js
import { monModule } from './monModule'
```

---

**export default**

Si on spécifie `default` à l'export du module 
(il ne peut y avoir qu'un seul `export default` par fichier)

```js
export default monModule
```

Alors on peut l'importer de cette manière

```js
import monModule from './monModule'
```

équivalent en ES5

```js
monModule = require('./monModule')
```

---

Dans un navigateur, pour utiliser des import export,
on doit préciser que le script est de type module :

`<script type="module">`

---

<!-- .slide: class="alternate" -->

# Étape 10

**Consignes**

* externaliser les classes dans un autre fichier JavaScript
* importer les classes du fichier externe
* utiliser un fichier de styles externe

**Prérequis**

* modules JS

<!--h-->

* [Revenir au sommaire](/slides/README.md#/2)
* [7. Stockage](/slides/7_JS_stockage.md)
