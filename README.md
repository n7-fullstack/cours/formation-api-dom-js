# N7 - Cours API DOM / API JavaScript

Ce repository contient le support de formation et le TP
concernant le module de cours "API DOM / API JS" 
de la formation FullStack donnée à l'N7.

Ces cours ont été conçus par Mathieu DARTIGUES,
enseignant dans la formation en 2020.

## Instructions de démarrage

```
npm ci
npm run watch
```

## Liens

TP : http://n7-nonogramme.surge.sh/

Slides : https://n7-api-dom-js.surge.sh/slides/README.html#/

Attention, les liens des pages HTML pointent sur des `.md`, 
je n'ai pas trouvé l'astuce pour que les liens pointent sur les fichiers `.html`.

Pensez à corriger l'URL de votre navigateur pour transformer le `.md` en `.html`.

## License

This work by Mathieu DARTIGUES is licensed under CC BY-NC-SA 4.0.

To view a copy of this license, visit https://creativecommons.org/licenses/by-nc-sa/4.0